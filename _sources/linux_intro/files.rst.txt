:tocdepth: 2

.. _ch_intro_files:

Working with Files
==================
 
.. contents:: :local:

Filenames and extensions
----------------------------

Filenames should be created to be meaningful, to remind their creator
of what they contain, and to allow any other people to have a clear
idea of what they contain.  For example, a file name "aaaa" is *not*
useful: it does not describe file contents, nor does it even reveal
what *type* of file it is.

By convention, filenames generally end with an **extension**,
separated by a period from the descriptive handle.  The extension is
typically 2-4 characters, and identifies the *kind* of file that it is
and therefore also what program might be able to open it to access its
contents.  There are several existing conventions for major categories
of files, and a few are demonstrated in the following table.

.. list-table:: 
   :header-rows: 1
   :widths: 15 50 35
      
   * - Extension
     - Content
     - Example program(s)
   * - pdf
     - "portable data format" files, common for forms (and supposed to
       be sharable across all operating systems and computers)
     - evince, Adobe Acrobat, ...
   * - doc
     - document files
     - LibreOffice Writer (libreoffice), Microsoft Word, ...
   * - txt
     - basic/unformatted text files
     - gedit, emacs, ...
   * - py
     - Python programs
     - gedit, emacs, jupyter-notebook, ...
   * - tex
     - LaTeX files
     - gedit, emacs, texmaker, ...
   * - jpg, png, tif
     - image files
     - eog 
   * - html
     - "hypertext markup language" files, common for webpages
     - firefox, chrome, safari, ...

Some of the above programs exist in standard Linux distributions by
default when they are installed.  Other ones have to be installed, for
example with a package manager (e.g., "Aptitude" on Ubuntu Linux).


Good file naming habits
---------------------------

There are several characters that have special meaning in Linux, and
so they should be avoided within filenames to avoid problems and
misinterpretation.  It is still possible to make meaningful, readable
names fairly easily, though.

**Good characters to use**
  * alphabet: a-z, A-Z
  * numbers: 0-9
  * period "." and underscore "_" (esp. to separate words)
  * plus "+" and minus (or hyphen) "-" (not so common, but fine to use)

**Bad characters to use**
  * space " " (typically separates *different* names, words, etc.)
  * asterisk "*" and question mark "?" (wildcard characters, discussed
    later)
  * exclamation "!"
  * square brackets "[" and "]"
  * curly brackets "{" and "}" 
  * forward slash "/" (separates names in path)
  * "$" (used to denote shell variables)
  * "#" (comment symbol)

While it is technically possible to have spaces in file names (and
other non-Unix based systems might do this often), it is a *very* bad
habit to get into-- instead of spaces, it is common to use the
underscore "_" or a period ".".

**Valid file names:** alg_assign_01.py, quiz_english.txt,
schedule_2018_09_03.doc, essay.final.v5.tex, HawkingStephen_BH.pdf

**Poor file names:** asdf (no description, no extension), tmp.txtpypdf
(unrecognized extension), 00.doc (undescriptive), reachforthe*.tex
("*" is a special character), "I Want To Learn Programming.pdf"
(spaces in filename).


